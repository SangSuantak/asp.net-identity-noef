﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Identity.Core;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
//using Microsoft.AspNet.Identity;
//using Microsoft.Owin.Security;
//using Microsoft.AspNet.WebApi

namespace Identity.Web.Controllers
{
    //public class AuthorizeTest : AuthorizationFilterAttribute
    //{
    //    private string _role;
                
    //    public AuthorizeTest(string role)
    //    {
    //        _role = role;
    //    }

    //    public override void OnAuthorization(HttpActionContext actionContext)
    //    {
    //        string abc = "test";
    //    }
    //}

    [RoutePrefix("api/role")]
    public class RoleController : ApiController
    {        
        private IdentityUserManager _userManager;
        private IdentityRoleManager _roleManager;

        public RoleController() : base() { }

        public RoleController(IdentityUserManager userManager, IdentityRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public IdentityUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<IdentityUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IdentityRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<IdentityRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        [HttpGet]
        [Authorize(Roles = "PortalAdmin")]
        [Route("")]
        public Task<IdentityRole> Get()
        {
            //if(id == null || id == default(Guid))
            //{
            //    throw new ArgumentNullException("Rold Id");
            //}

            //var result = await RoleManager.FindByIdAsync(id);

            var req = Request;

            return Task.FromResult(new IdentityRole());
        }

    }
}
