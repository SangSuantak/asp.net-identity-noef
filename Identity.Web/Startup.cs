﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Identity.Web.Startup))]
namespace Identity.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Test commit from branch
            ConfigureAuth(app);
        }
    }
}
