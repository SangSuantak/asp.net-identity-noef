﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Identity.Core
{
    public class RoleStore<TRole> : IRoleStore<TRole, Guid>, IQueryableRoleStore<TRole, Guid>
        where TRole : IdentityRole
    {
        private string _connection;
                
        public RoleStore(string connection)
        {
            _connection = connection;
        }

        public Task CreateAsync(TRole role)
        {
            if (role == null)
                throw new ArgumentNullException("role");
            
            if (role.RoleId == default(Guid))
                role.RoleId = Guid.NewGuid();

            using (var connection = new SqlConnection(_connection))
            {
                using (var cmd = new SqlCommand("[dbo].[usp_iden_CreateRole]", connection))
                {
                    if(connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RoleId", role.RoleId);
                    cmd.Parameters.AddWithValue("@Name", role.Name);
                    return Task.FromResult(cmd.ExecuteNonQueryAsync());
                }
            }
        }

        public Task DeleteAsync(TRole role)
        {
            if (role == null)
                throw new ArgumentNullException("role");

            string sql = @"
DELETE FROM IdentityRole 
WHERE RoleId=@ROLEID";

            using (var connection = new SqlConnection(_connection))
                return Task.FromResult(connection.Execute(sql, role));
        }

        public Task<TRole> FindByIdAsync(Guid roleId)
        {
            using (var connection = new SqlConnection(_connection))
            {
                using (var cmd = new SqlCommand("[dbo].[usp_iden_FindRoleById]", connection))
                {
                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RoleId", roleId);
                    using(var reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        IdentityRole role = null;

                        if (reader.HasRows)
                        {
                            role = new IdentityRole();
                            while (reader.Read())
                            {
                                role.RoleId = roleId;
                                role.Name = Convert.ToString(reader["Name"]);
                            }
                        }

                        return Task.FromResult((TRole)role);
                    }
                }
            }
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            using (var connection = new SqlConnection(_connection))
            {
                using (var cmd = new SqlCommand("[dbo].[usp_iden_FindRoleByName]", connection))
                {
                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Name", roleName);
                    using (var reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        IdentityRole role = null;

                        if (reader.HasRows)
                        {
                            role = new IdentityRole();
                            while (reader.Read())
                            {
                                role.RoleId = reader.GetGuid(reader.GetOrdinal("RoleId"));
                                role.Name = roleName;
                            }
                        }

                        return Task.FromResult((TRole)role);
                    }
                }
            }
        }

        public Task UpdateAsync(TRole role)
        {
            if (role == null)
                throw new ArgumentNullException("role");

            string sql = @"
UPDATE IdentityRole SET 
	[Name] = @NAME
WHERE 
    RoleId = @ROLEID";

            using (var connection = new SqlConnection(_connection))
                return Task.FromResult(connection.Execute(sql, role));
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                _connection = null;
            }
        }

        /* IQueryableRoleStore
        ---------------------------*/
        /// <summary>
        /// Get all roles
        /// </summary>
        /// <returns></returns>
        public IQueryable<TRole> Roles()
        {
            using (var connection = new SqlConnection(_connection))
            {
                using (var cmd = new SqlCommand("[dbo].[usp_iden_GetRoles]", connection))
                {
                    if (connection.State == System.Data.ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        List<IdentityRole> roles = new List<IdentityRole>();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                roles.Add(new IdentityRole {
                                    RoleId = reader.GetGuid(reader.GetOrdinal("RoleId")),
                                    Name = Convert.ToString(reader["Name"])
                                });                                
                            }
                        }
                        
                        return  ((IEnumerable<TRole>)roles).AsQueryable();
                    }
                }
            }
        }

        IQueryable<TRole> IQueryableRoleStore<TRole, Guid>.Roles
        {
            get
            {
                return Roles();
            }
        }
    }
}
